# Jumper Wire organiser

![](pictures/DSC_7914.jpg)

Easy access sorted jumper wire organiser with an easy mount to a pegboard.


***Features***
* Has separations to accommodate different lengths.
* Also it can be mounted on the peg board
Or any board.

It might look bukly at first..
but here are my two other prototypes...
![](pictures/DSC_7836.jpg)
![](pictures/DSC_7835.jpg)
 In hot summer days they did not last.. it was PLA tho.. this time PETG and way bulkier.

I also glued mine into place.. Press Fit was ok.. It could be made larger mountings to be fine with friction fit only...


***BOM***

![](pictures/DSC_7856.jpg)

Some self tapping screws to mount.

![](pictures/DSC_7840.jpg)
![](pictures/DSC_7519.jpg)
![](pictures/DSC_7850.jpg)
![](pictures/DSC_7910.jpg)
